/// <summary>
/// @mainpage Orc Game using Polymorphism, Inhenritance and encapsulation
/// @Author Rafael Girao
/// @Version 1.0
/// @brief Program will be a duel between Orcs vs Trolls
///
/// Time Spent:
///		10th/10/2016 09:30 90min (1hr30min)
///		10th/10/2016 18:00 240min (4hr)
///		11st/10/2016 16:00 360min (6hr)
///		12nd/10/2016 19:00 150min (2hr30min)
///		13rd/10/2016 13:00 240min (4hr)
///		13rd/10/2016 18:00 120min (2hr)
///		14th/10/2016 17:00 180min (3hr)
///		15th/10/2016 14:00 360min (6hr)
///		16th/10/2016 10:00 480min (8hr)
///
/// Total Time Taken:
///		37hr 00min
/// </summary>

#include "Game.h"

/// <summary>
/// @brief Starting point for all C++ Programs
/// 
/// Create game object and runs it
/// </summary>

int main()
{
	Game game;
	
	game.run();

	//game.~Game();
	return 0;
}