#pragma once

#include <iostream>
#include <Windows.h>
#include "Lifeform.h"
#include "Orc.h"
#include "Troll.h"
#include "Constants.h"

class Game
{
public:
	Game();
	~Game();
	void run();

protected:
	/* MEMBER DECLARATIONS **/

	/* FUNCTIONS **/

	void clear();
	void display(const char * s) const;
	void display(const std::string s[], const unsigned & length) const;
	void newLine();
	void input(int * l_input);
	void delay(int l_delayAmount);
	void pause();

	void choose(Choose choice);
	bool checkInput(Choose choice, int * l_input);
	bool checkInput(Lifeform * l_turn, Lifeform * l_waiting, int * l_input);
	void fight();

	/* CONSTANTS **/

	const Constants CON;

	/* VARIABLES **/

	Race m_race;
	Classes m_class;
	Turn m_turn;
	int * m_input;
	int m_dmg;
	std::string m_name;
	Lifeform * m_player;
	Lifeform * m_enemy;
};

