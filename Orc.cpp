#include "Orc.h"



Orc::Orc() :
	Lifeform()
{
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

Orc::~Orc()
{
	m_attacks.~vector();
	m_defences.~vector();
}

Orc::Orc(Classes l_class, std::string l_name, bool l_alive, bool l_isPlayer, int l_health, int l_mana, std::string l_type) :
	Lifeform(l_name, l_alive, l_isPlayer, l_health, l_mana, l_type)
{
	switch (l_class)
	{
	case Classes::Warrior:
		m_attacks = std::vector<Attack>(1u, ATT.GREATSWORD);
		m_attacks.push_back(ATT.AXE);
		m_defences = std::vector<Defence>(1u, DEF.FULL_PLATE);
		m_defences[0].hasShield(true);
		break;
	case Classes::Mage:
		m_attacks = std::vector<Attack>(1u, ATT.ARCANE_MISSILE);
		m_attacks.push_back(ATT.FIREBALL);
		m_attacks.push_back(ATT.ELECTRIC_CONE);
		m_attacks.push_back(ATT.BOULDER_THROW);
		m_defences = std::vector<Defence>(1u, DEF.CHAIN_MAIL);
		m_defences[0].hasShield(false);
		break;
	case Classes::Ranger:
		m_attacks = std::vector<Attack>(1u, ATT.LONGSWORD);
		m_attacks.push_back(ATT.SHORTSWORD);
		m_attacks.push_back(ATT.BOW);
		m_defences = std::vector<Defence>(1u, DEF.BREASTPLATE);
		m_defences[0].hasShield(false);
		break;
	default:
		break;
	}
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

Orc::Orc(std::string l_name, Classes l_class, bool l_isPlayer) :
	Lifeform(l_name, true, l_isPlayer, 1, 1, "Orc")
{
	switch (l_class)
	{
	case Classes::Warrior:
		setHealth(1200);
		setMana(0);
		m_attacks = std::vector<Attack>(1u, ATT.GREATSWORD);
		m_attacks.push_back(ATT.AXE);
		m_defences = std::vector<Defence>(1u, DEF.FULL_PLATE);
		m_defences[0].hasShield(true);
		break;
	case Classes::Mage:
		setHealth(1000);
		setMana(1000);
		m_attacks = std::vector<Attack>(1u, ATT.ARCANE_MISSILE);
		m_attacks.push_back(ATT.FIREBALL);
		m_attacks.push_back(ATT.ELECTRIC_CONE);
		m_attacks.push_back(ATT.BOULDER_THROW);
		m_defences = std::vector<Defence>(1u, DEF.CHAIN_MAIL);
		m_defences[0].hasShield(false);
		break;
	case Classes::Ranger:
		setHealth(1100);
		setMana(0);
		m_attacks = std::vector<Attack>(1u, ATT.LONGSWORD);
		m_attacks.push_back(ATT.SHORTSWORD);
		m_attacks.push_back(ATT.BOW);
		m_defences = std::vector<Defence>(1u, DEF.BREASTPLATE);
		m_defences[0].hasShield(false);
		break;
	default:
		break;
	}
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

std::string Orc::getStats()
{
	std::string s;
	s += "------------------------\n";
	s += " " + m_name + " " + TYPE + " :\n";
	if (m_alive)
	{
		s += "  - HP : " + std::to_string(m_hp) + "\\" + std::to_string(m_maxHp) + "\n";
		if (m_maxMp > 0)
		{
			s += "  - MP : " + std::to_string(m_mp) + "\\" + std::to_string(m_maxMp) + "\n";
		}
		if (m_isPlayer)
		{
			s += "    - Attacks:\n     ";
			for (unsigned i = 0u; i < m_attacks.size(); i++)
			{
				s += " " + std::to_string(i + 1) + "- " + m_attacks[i].name() + "; ";
			}
			s += "\n    - Defences:\n     ";
			for (unsigned i = 0u; i < m_defences.size(); i++)
			{
				s += " - " + m_defences[i].name() + ";  ";
			}
			s += "\n";
		}

	}
	else
	{
		for (unsigned i = 0u; i < CON.TEXT.LIFEFORM_DEAD_SIZE; i++)
		{
			s += CON.TEXT.LIFEFORM_DEAD[i];
		}
	}

	return s;
}

bool Orc::attack(Lifeform * l_target, Attack * l_attack)
{
	if (l_attack->cost() != 0)
	{
		if (l_attack->cost() <= m_mp)
		{
			m_mp -= l_attack->cost();
		}
		else
		{
			return false;
		}
	}
	return l_target->damage(*l_attack);
}

bool Orc::damage(Attack l_attack)
{
	int hitChance = l_attack.target();
	int dmg = l_attack.attack();
	int defence = 0;

	for (unsigned i = 0u; i < m_defences.size(); i++)
	{
		if (m_defences[i].hasShield())
		{
			defence += 3 + m_defences[i].defend();
		}
		else
		{
			defence += m_defences[i].defend();
		}
	}

	if (l_attack.cost() == 0)
	{
		if (hitChance < defence + (rand() % 10 - 5))
		{
			dmg -= (rand() % 30 + 1);
		}
	}
	else
	{
		if (hitChance < defence + (rand() % 20 - 10))
		{
			dmg -= (rand() % 30 + 1);
		}

	}
	if (dmg > 0)
	{
		m_hp -= dmg;
		return true;
	}
	else
	{
		return false;
	}
}
