#pragma once

#include "Text.h"

enum class Choose
{
	Race, Class, Name, Enemy
};

enum class Race
{
	Orc = 1, Troll = 2
};

enum class Turn
{
	Player, Enemy
};

enum class FightState
{
	Fighting, DeadEnemy, DeadPlayer
};

enum class Ability
{
	First = 1, Second, Third, Fourth
};

const struct Constants
{
	Text TEXT;
	const int ENEMY_THINK_TIME = 1200;	// in miliseconds
	const int ENEMY_TURN_TIME = 3000;	// in miliseconds
};

