#include "Attack.h"

Attack::Attack(std::string l_name, int l_hit, int l_damage, int l_mpCost) :
	m_name(l_name),
	m_hit( l_hit ),
	m_damage( l_damage ),
	m_mpCost(l_mpCost)
{
}

std::string Attack::name() const
{
	return m_name;
}

int Attack::target() const
{
	return m_hit;
}

int Attack::attack() const
{
	return m_damage;
}

int Attack::cost() const
{
	return m_mpCost;
}
