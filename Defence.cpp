#include "Defence.h"

Defence::Defence(std::string l_name, int l_ac, bool l_shield) :
	m_name( l_name ),
	m_ac( l_ac ),
	m_shield( l_shield )
{
}

int Defence::defend()
{
	return m_ac;
}

std::string Defence::name() const
{
	return m_name;
}

bool Defence::hasShield() const
{
	return m_shield;
}

void Defence::hasShield(bool giveShield)
{
	m_shield = giveShield;
}
