#include "Troll.h"



Troll::Troll() :
	Lifeform()
{
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

Troll::~Troll()
{
	m_attacks.~vector();
	m_defences.~vector();
}

Troll::Troll(Classes l_class, std::string l_name, bool l_alive, bool l_isPlayer, int l_health, int l_mana) :
	Lifeform(l_name, l_alive, l_isPlayer, l_health, l_mana, "Troll")
{
	switch (l_class)
	{
	case Classes::Warrior:
		m_attacks = std::vector<Attack>(1u, ATT.WARHAMMER);
		m_attacks.push_back(ATT.CLUB);
		m_defences = std::vector<Defence>(1u, DEF.CHAIN_MAIL);
		m_defences[0].hasShield(true);
		break;
	case Classes::Mage:
		m_attacks = std::vector<Attack>(1u, ATT.ARCANE_MISSILE);
		m_attacks.push_back(ATT.FIREBALL);
		m_attacks.push_back(ATT.ELECTRIC_CONE);
		m_attacks.push_back(ATT.BOULDER_THROW);
		m_defences = std::vector<Defence>(1u, DEF.ROBE);
		m_defences[0].hasShield(false);
		break;
	case Classes::Ranger:
		m_attacks = std::vector<Attack>(1u, ATT.AXE);
		m_attacks.push_back(ATT.SHORTSWORD);
		m_attacks.push_back(ATT.BOW);
		m_defences = std::vector<Defence>(1u, DEF.LEATHER_ARMOR);
		m_defences[0].hasShield(true);
		break;
	default:
		break;
	}
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

Troll::Troll(std::string l_name, Classes l_class, bool l_isPlayer) :
	Lifeform(l_name, true, l_isPlayer, 1, 1, "Troll")
{
	switch (l_class)
	{
	case Classes::Warrior:
		setHealth(1000);
		setMana(0);
		m_attacks = std::vector<Attack>(1u, ATT.WARHAMMER);
		m_attacks.push_back(ATT.CLUB);
		m_defences = std::vector<Defence>(1u, DEF.CHAIN_MAIL);
		m_defences[0].hasShield(true);
		break;
	case Classes::Mage:
		setHealth(800);
		setMana(2000);
		m_attacks = std::vector<Attack>(1u, ATT.ARCANE_MISSILE);
		m_attacks.push_back(ATT.FIREBALL);
		m_attacks.push_back(ATT.ELECTRIC_CONE);
		m_attacks.push_back(ATT.BOULDER_THROW);
		m_defences = std::vector<Defence>(1u, DEF.ROBE);
		m_defences[0].hasShield(false);
		break;
	case Classes::Ranger:
		setHealth(900);
		setMana(0);
		m_attacks = std::vector<Attack>(1u, ATT.AXE);
		m_attacks.push_back(ATT.SHORTSWORD);
		m_attacks.push_back(ATT.BOW);
		m_defences = std::vector<Defence>(1u, DEF.LEATHER_ARMOR);
		m_defences[0].hasShield(true);
		break;
	default:
		break;
	}
	srand(static_cast<unsigned int>(time(NULL))); // initialize random seed
}

std::string Troll::getStats()
{
	std::string s;
	s += "------------------------\n";
	s += " " + m_name + " " + TYPE + " :\n";
	if (m_alive)
	{
		s += "  - HP : " + std::to_string(m_hp) + "\\" + std::to_string(m_maxHp) + "\n";
		if (m_maxMp > 0)
		{
			s += "  - MP : " + std::to_string(m_mp) + "\\" + std::to_string(m_maxMp) + "\n";
		}
		if (m_isPlayer)
		{
			s += "    - Attacks:\n     ";
			for (unsigned i = 0u; i < m_attacks.size(); i++)
			{
				s += " " + std::to_string(i + 1) + "- " + m_attacks[i].name() + "; ";
			}
			s += "\n    - Defences:\n     ";
			for (unsigned i = 0u; i < m_defences.size(); i++)
			{
				s += " - " + m_defences[i].name() + ";  ";
			}
			s += "\n";
		}
	}
	else
	{
		for (unsigned i = 0u; i < CON.TEXT.LIFEFORM_DEAD_SIZE; i++)
		{
			s += CON.TEXT.LIFEFORM_DEAD[i];
		}
	}

	return s;
}

bool Troll::attack(Lifeform * l_target, Attack * l_attack)
{
	if (l_attack->cost() != 0)
	{
		if (l_attack->cost() <= m_mp)
		{
			m_mp -= l_attack->cost();
		}
		else
		{
			return false;
		}
	}
	return l_target->damage(*l_attack);
}

bool Troll::damage(Attack l_attack)
{
	int hitChance = l_attack.target();
	int dmg = l_attack.attack();
	int defence = 0;
	int dodge = rand() % 10 + 1; // generates a value between 1 to 10

	for (unsigned i = 0u; i < m_defences.size(); i++)
	{
		if (m_defences[i].hasShield())
		{
			defence += 5 + m_defences[i].defend();
		}
		else
		{
			defence += m_defences[i].defend();
		}
	}
	if (l_attack.cost() == 0)
	{
		if (hitChance < (defence + dodge) + (rand() % 10 - 5))
		{
			dmg -= (rand() % 20 + 1);
		}
	}
	else
	{
		if (hitChance * 2 < (defence + dodge) + (rand() % 20 - 5))
		{
			dmg -= (rand() % 20 + 1);
		}
	}
	
	if (dmg > 0)
	{
		m_hp -= dmg;
		return true;
	}
	else
	{
		return false;
	}
}
