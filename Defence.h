#pragma once

#include <string>

class Defence
{
public:
	Defence(std::string l_name = "Armor" , int l_ac = 0, bool l_shield = false);
	virtual int defend();
	std::string name() const;
	bool hasShield() const;
	void hasShield(bool giveShield);
protected:
	std::string m_name;	// Name of armor
	int m_ac;			// Armor class
	bool m_shield;		// whether or not a shield is possessed
};
