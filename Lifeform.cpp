#include "Lifeform.h"

Lifeform::Lifeform(std::string l_name, bool l_alive, bool l_isPlayer, int l_health, int l_mana, std::string l_type) :
	TYPE(l_type),
	m_name( l_name ),
	m_alive( l_alive ),
	m_isPlayer( l_isPlayer ),
	m_maxHp( l_health ),
	m_hp( l_health ),
	m_maxMp( l_mana ),
	m_mp( l_mana )
{
}

bool Lifeform::getAlive() const
{
	return (m_hp > 0);
}

std::string Lifeform::getName() const
{
	return m_name;
}

int Lifeform::getMaxHealth() const
{
	return m_maxHp;
}

int Lifeform::getHealth() const
{
	return m_hp;
}

int Lifeform::getMaxMana() const
{
	return m_maxMp;
}

int Lifeform::getMana() const
{
	return m_mp;
}

std::vector<Attack> Lifeform::getAttacks() const
{
	return m_attacks;
}

std::vector<Defence> Lifeform::getDefences() const
{
	return m_defences;
}

void Lifeform::setHealth(int l_hp)
{
	m_maxHp = l_hp;
	m_hp = l_hp;
}

void Lifeform::setMana(int l_mp)
{
	m_maxMp = l_mp;
	m_mp = l_mp;
}

std::string Lifeform::getStats()
{
	std::string s;
	s += "------------------------\n";
	s += " " + m_name + " " + TYPE + " :\n";
	if (m_alive)
	{
		s += "  - HP : " + std::to_string(m_hp) + "\\" + std::to_string(m_maxHp) + "\n";
		if (m_maxMp > 0)
		{
			s += "  - MP : " + std::to_string(m_mp) + "\\" + std::to_string(m_maxMp) + "\n";
		}
	}
	else
	{
		s += "\tDEAD\n";
	}

	return s;
}
