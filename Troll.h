#pragma once

#include "Lifeform.h"
#include "Classes.h"
#include "Attacks.h"
#include "Defences.h"
#include <stdio.h>
#include <random>
#include <time.h>

class Troll : public Lifeform
{
public:
	Troll();
	~Troll();
	Troll(Classes l_class, std::string l_name = "", bool l_alive = true, bool l_isPlayer = false, int l_health = 1, int l_mana = 1);
	Troll(std::string l_name, Classes l_class, bool l_isPlayer);

	std::string getStats() override;
	bool attack(Lifeform * l_target, Attack * l_attack) override;
	bool damage(Attack l_attack) override;
private:
	Attacks ATT;
	Defences DEF;
};
