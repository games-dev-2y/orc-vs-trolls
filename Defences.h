#pragma once

#include "Defence.h"

const struct Defences
{
	Defence FULL_PLATE = Defence::Defence("Full-Plate Armor", 10);
	Defence HALF_PLATE = Defence::Defence("Half-Plate Armor", 8);
	Defence BREASTPLATE = Defence::Defence("Breastplate Armor", 6);
	Defence CHAIN_MAIL = Defence::Defence("Chain Mail Armor", 4);
	Defence LEATHER_ARMOR = Defence::Defence("Leather Armor", 2);
	Defence ROBE = Defence::Defence("Robe", 0);
};