#pragma once

struct Text
{
	static const unsigned GAME_TITLE_SIZE = 6u;
	std::string GAME_TITLE[GAME_TITLE_SIZE] =
	{
		"  ____                __      _______   _______        _ _\n",
		" / __ \\               \\ \\    / / ____| |__   __|      | | |\n",
		"| |  | |_ __ ___ ___   \\ \\  / / (___      | |_ __ ___ | | |___\n",
		"| |  | | '__/ __/ __|   \\ \\/ / \\___ \\     | | '__/ _ \\| | / __|\n",
		"| |__| | | | (__\\__ \\    \\  /  ____) |    | | | | (_) | | \\__ \\\n",
		" \\____/|_|  \\___|___/     \\/  |_____/     |_|_|  \\___/|_|_|___/\n\n"
	};
	static const unsigned CHOOSE_RACE_SIZE = 5u;
	std::string CHOOSE_RACE[CHOOSE_RACE_SIZE] =
	{
		"CHOOSE YOUR OWN RACE\n",
		"  Races:\n",
		"    1 - Orcs (High hp, low mp)\n",
		"    2 - Trolls (Low hp, high mp)\n",
		"      Option: "
	};
	static const unsigned CHOOSE_CLASS_SIZE = 7u;
	std::string CHOOSE_CLASS[CHOOSE_CLASS_SIZE] =
	{
		"CHOOSE YOUR OWN CLASS\n",
		"  Classes:\n",
		"    1 - Warrior    (lots of melee weps, weak at long range)\n",
		"    2 - Mage       (lots of spells, weak at close range)\n",
		"    3 - Ranger     (low mp dependancy, jack-of-all-trades)\n",
		"    Options: "
	};
	static const unsigned CHOOSE_NAME_SIZE = 2u;
	std::string CHOOSE_NAME[CHOOSE_NAME_SIZE] =
	{
		"ENTER CHARACTER'S NAME:\n",
		" Name: "
	};
	static const unsigned CHOOSE_ENEMY_SIZE = 7u;
	std::string CHOOSE_ENEMY[CHOOSE_ENEMY_SIZE] =
	{
		"CHOOSE YOUR ENEMY:\n",
		" ",
		" ENEMY CLASS:\n",
		"    1 - Warrior    (lots of melee weps, weak at long range)\n",
		"    2 - Mage       (lots of spells, weak at close range)\n",
		"    3 - Ranger     (low mp dependancy, jack-of-all-trades)\n",
		"    Options: "
	};
	static const unsigned GAME_OVER_SIZE = 8u;
	std::string GAME_OVER[GAME_OVER_SIZE] = 
	{
		"_______  _______  _______  _______    _______           _______  _______\n",
		"(  ____ \\(  ___  )(       )(  ____ \\  (  ___  )|\\     /|(  ____ \\(  ____ )\n",
		"| (    \\/| (   ) || () () || (    \\/  | (   ) || )   ( || (    \\/| (    )|\n",
		"| |      | (___) || || || || (__      | |   | || |   | || (__    | (____)|\n",
		"| | ____ |  ___  || |(_)| ||  __)     | |   | |( (   ) )|  __)   |     __)\n",
		"| | \\_  )| (   ) || |   | || (        | |   | | \\ \\_/ / | (      | (\\ (\n",
		"| (___) || )   ( || )   ( || (____/\\  | (___) |  \\   /  | (____/\\| ) \\ \\__\n",
		"(_______)|/     \\||/     \\|(_______/  (_______)   \\_/   (_______/|/   \\__/"
	};
	static const unsigned LIFEFORM_DEAD_SIZE = 8u;
	std::string LIFEFORM_DEAD[LIFEFORM_DEAD_SIZE] =
	{
		"______   _______  _______  ______\n",
		"(  __  \\ (  ____ \\(  ___  )(  __  \\\n",
		"| (  \\  )| (    \\/| (   ) || (  \\  )\n",
		"| |   ) || (__    | (___) || |   ) |\n",
		"| |   | ||  __)   |  ___  || |   | |\n",
		"| |   ) || (      | (   ) || |   ) |\n",
		"| (__/  )| (____/\\| )   ( || (__/  )\n",
		"(______/ (_______/|/     \\|(______/\n"
	};
	std::string FIGHT_PICK =
		"Pick an attack\n  Option: ";
	std::string FIGHT_ENEMY =
		"Enemy is planning his move\n";
	std::string FIGHT_ENEMY_THINKING = ".";
	std::string FIGHT_ENEMY_ATTACK =
		"Enemy has attacked using his ";
};