#include "Game.h"



Game::Game()
{
	m_input = new int();
	m_dmg = 0;
}


Game::~Game()
{
	delete m_player;
	delete m_enemy;
	delete m_input;
}


void Game::run()
{
	choose(Choose::Race);
	choose(Choose::Class);
	choose(Choose::Name);
	choose(Choose::Enemy);
	fight();
}


void Game::clear()
{
	std::system("CLS");
	display(CON.TEXT.GAME_TITLE, CON.TEXT.GAME_TITLE_SIZE);
}

void Game::display(const char * s) const
{
	std::cout << s;
}

void Game::display(const std::string s[], const unsigned & length) const
{
	for (unsigned i = 0; i < length; i++)
	{
		display(s[i].c_str());
	}
}

void Game::newLine()
{
	std::cout << std::endl;
}

void Game::input(int * l_input)
{
	std::cin >> *l_input;
}

void Game::delay(int l_delayAmount)
{
	Sleep(l_delayAmount);
}

void Game::pause()
{
	std::system("PAUSE");
}

void Game::choose(Choose choice)
{
	bool picked = false;
	switch (choice)
	{
	case Choose::Race:
		while (picked == false)
		{
			clear();
			newLine();
			display(CON.TEXT.CHOOSE_RACE, CON.TEXT.CHOOSE_RACE_SIZE);
			input(m_input);
			picked = checkInput(choice, m_input);
		}
		break;
	case Choose::Class:
		while (picked == false)
		{
			clear();
			newLine();
			display(CON.TEXT.CHOOSE_CLASS, CON.TEXT.CHOOSE_CLASS_SIZE);
			input(m_input);
			picked = checkInput(choice, m_input);
		}
		break;
	case Choose::Name:
		while (picked == false)
		{
			clear();
			newLine();
			display(CON.TEXT.CHOOSE_NAME, CON.TEXT.CHOOSE_NAME_SIZE);
			picked = checkInput(choice, m_input);
		}
		break;
	case Choose::Enemy:
		while (picked == false)
		{
			clear();
			newLine();
			for (unsigned i = 0u; i < CON.TEXT.CHOOSE_ENEMY_SIZE; i++)
			{
				if (i == 1u)
				{
					std::string s = CON.TEXT.CHOOSE_ENEMY[i];
					switch (m_race)
					{
					case Race::Orc:
						s += "TROLL";
						display(s.c_str());
						break;
					case Race::Troll:
						s += "ORC";
						display(s.c_str());
						break;
					}
				}
				else
				{
					display(CON.TEXT.CHOOSE_ENEMY[i].c_str());
				}
			}
			input(m_input);
			picked = checkInput(choice, m_input);
		}
		break;
	default:
		break;
	}
}

bool Game::checkInput(Choose choice, int * l_input)
{
	bool correctPick = false;
	switch (choice)
	{
	case Choose::Race:
		switch (*l_input)
		{
		case static_cast<int>(Race::Orc) :		// picked 1
		case static_cast<int>(Race::Troll) :	// picked 2
			m_race = static_cast<Race>(*l_input);
			correctPick = true;
			break;
		default:
			break;
		}
		break;
	case Choose::Class:
		switch (*l_input)
		{
		case static_cast<int>(Classes::Warrior) :	// picked 1
		case static_cast<int>(Classes::Mage) :		// picked 2
		case static_cast<int>(Classes::Ranger) :	// picked 3
			m_class = static_cast<Classes>(*l_input);
			correctPick = true;
			break;
		default:
			break;
		}
		break;
	case Choose::Name:
		std::cin.ignore();
		std::getline(std::cin, m_name);
		switch (m_race)
		{
		case Race::Orc:
			m_player = new Orc(m_name, m_class, true);
			break;
		case Race::Troll:
			m_player = new Troll(m_name, m_class, true);
			break;
		default:
			break;
		}
		correctPick = true;
		break;
	case Choose::Enemy:
		switch (*l_input)
		{
		case static_cast<int>(Classes::Warrior) :	// picked 1
		case static_cast<int>(Classes::Mage) :		// picked 2
		case static_cast<int>(Classes::Ranger) :	// picked 3
			switch (m_race) // Enemy will be from opposite race
			{
			case Race::Orc:
				m_enemy = new Troll("", static_cast<Classes>(*l_input), false);
				break;
			case Race::Troll:
				m_enemy = new Orc("", static_cast<Classes>(*l_input), false);
				break;
			default:
				break;
			}
			correctPick = true;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return correctPick;
}

bool Game::checkInput(Lifeform * l_turn, Lifeform * l_waiting, int * l_input)
{
	bool correctPick = false;
	std::string attMsg;

	if (*l_input > 0 && *l_input <= l_turn->getAttacks().size())
	{
		m_dmg = l_waiting->getHealth();
		l_turn->attack( l_waiting, &( l_turn->getAttacks()[ *(l_input)-1 ] ) );
		m_dmg -= l_waiting->getHealth();
		if (l_turn->getAttacks()[*(l_input)-1].cost() == 0)
		{
			attMsg = "The " + l_turn->getAttacks()[*(l_input)-1].name() + " attack did " + std::to_string(m_dmg) + " dmg\n";
		}
		else
		{
			attMsg = "The " + l_turn->getAttacks()[*(l_input)-1].name() + " spell did " + std::to_string(m_dmg) + " dmg";
			attMsg += " and cost " + std::to_string(l_turn->getAttacks()[*(l_input)-1].cost()) + " mp\n";
		}
		display(attMsg.c_str());
		correctPick = true;
	}

	return correctPick;
}

void Game::fight()
{
	FightState l_fightState = FightState::Fighting;
	std::string l_enemyMsg;
	Attack l_choosenAttack;
	m_turn = Turn::Player;
	while (l_fightState == FightState::Fighting)
	{
		clear();
		newLine();
		display(m_enemy->getStats().c_str());
		newLine();
		display(m_player->getStats().c_str());
		newLine();
		switch (m_turn)
		{
		case Turn::Player:
			display(CON.TEXT.FIGHT_PICK.c_str());
			input(m_input);
			if (checkInput(m_player, m_enemy, m_input) == true)
			{
				m_turn = Turn::Enemy;
				pause();
			}
			else
			{
				m_turn = Turn::Player;
			}
			break;
		case Turn::Enemy:
			l_enemyMsg = CON.TEXT.FIGHT_ENEMY_ATTACK;
			l_choosenAttack = m_enemy->getAttacks().front(); // auto pick 1st attack
			for (int i = 1; i < m_enemy->getAttacks().size(); i++)
			{
				if ( l_choosenAttack.attack() < m_enemy->getAttacks()[i].attack() &&
					m_enemy->getAttacks()[i].attack() <= m_player->getHealth())
				{
					l_choosenAttack = m_enemy->getAttacks()[i];
				}
			}
			m_dmg = m_player->getHealth();
			m_enemy->attack(m_player, &l_choosenAttack);
			m_dmg -= m_player->getHealth();
			l_enemyMsg += l_choosenAttack.name() + "\n";
			l_enemyMsg += "It did " + std::to_string(m_dmg) + " damage\n";
			display(l_enemyMsg.c_str());
			newLine();
			pause();

			m_turn = Turn::Player;
			break;
		}
		

		if (m_player->getAlive() != true)
		{
			l_fightState = FightState::DeadPlayer;
			newLine();
			display(CON.TEXT.GAME_OVER, CON.TEXT.GAME_OVER_SIZE);
			newLine();
			display("Thanks for playing !\n");
			newLine();
			pause();
		}
		if (m_enemy->getAlive() != true)
		{
			l_fightState = FightState::DeadEnemy;
			newLine();
			display("Enemy is\n");
			display(CON.TEXT.LIFEFORM_DEAD, CON.TEXT.LIFEFORM_DEAD_SIZE);
			newLine();
			display("Thanks for playing !\n");
			newLine();
			pause();
		}
	}
}
