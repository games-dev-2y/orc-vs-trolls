#pragma once

#include <string>
#include <vector>
#include "Attack.h"
#include "Defence.h"
#include "Constants.h"

class Lifeform
{
	//		Constructors		//
public:
	Lifeform(std::string l_name = "", bool l_alive = true, bool l_isPlayer = false, int l_health = 1, int l_mana = 1, std::string l_type = "Lifeform");

	//		Functions		//

	// Get functions

	bool getAlive() const;
	std::string getName() const;
	int getMaxHealth() const;
	int getHealth() const;
	int getMaxMana() const;
	int getMana() const;
	std::vector<Attack> getAttacks() const;
	std::vector<Defence> getDefences() const;

	// Set functions

	/*
	void setAlive(bool l_alive);
	void setName(std::string l_name);
	/**/
	void setHealth(int l_hp);
	void setMana(int l_mp);

	// Virtual functions

	virtual std::string getStats();
	virtual bool attack(Lifeform * l_target, Attack * l_attack) = 0;	// must be implemented in all derived classes
	virtual bool damage(Attack l_attack) = 0;							// must be implemented in all derived classes

	//		Constants		//

	const std::string TYPE;
	Constants CON;

protected:
	// member variables

	bool m_isPlayer;					// True if lifeform is player else false
	bool m_alive;						// True if alive else false
	std::string m_name;					// contains lifeform's name
	int m_maxHp;						// largest amount of health possible
	int m_hp;							// number damage it can take before dying
	int m_maxMp;						// largest amount of mana possible
	int m_mp;							// resource expended when spells are cast
	std::vector<Attack> m_attacks;		// stack that will contain all our various attacks
	std::vector<Defence> m_defences;	// stack that will contain all our methods of defence
	
};