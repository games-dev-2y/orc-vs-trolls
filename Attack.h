#pragma once

#include <string>

class Attack
{
public:
	Attack(std::string l_name = "Attack", int l_hit = 0, int l_damage = 0, int l_mpCost = 0);
	std::string name() const;
	int target() const;
	int attack() const;
	int cost() const;

protected:
	std::string m_name;
	int m_hit;
	int m_damage;
	int m_mpCost;
};