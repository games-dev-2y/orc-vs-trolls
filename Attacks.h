#pragma once

#include "Attack.h"

const struct Attacks
{
	Attack AXE = Attack::Attack("Axe", 5, 70);
	Attack SHORTSWORD = Attack::Attack("Shortsword", 8, 50);
	Attack LONGSWORD = Attack::Attack("Longsword", 7, 60);
	Attack GREATSWORD = Attack::Attack("Greatsword", 4, 80);
	Attack WARHAMMER = Attack::Attack("Warhammer", 1, 100);
	Attack CLUB = Attack::Attack("Club", 6, 60);
	Attack BOW = Attack::Attack("Bow", 10, 20);
	Attack FIREBALL = Attack::Attack("Fireball", 14, 70, 90);
	Attack ARCANE_MISSILE = Attack::Attack("Arcane Missile", 20, 40, 30);
	Attack ELECTRIC_CONE = Attack::Attack("Electric Cone", 16, 50, 60);
	Attack BOULDER_THROW = Attack::Attack("Boulder Throw", 10, 100, 150);
};